# Version 0.1.2

* [#8](https://gitlab.com/garybell/password-validation/-/issues/8) Addition of concise changelog at the root of the project.
* [#2](https://gitlab.com/garybell/password-validation/-/issues/2) Update the README.md to include installation and usage instructions.
