# Version 1.0.0

* [#5](https://gitlab.com/garybell/password-validation/-/issues/5) Add unit test pipeline for PHP 7.3
* [#4](https://gitlab.com/garybell/password-validation/-/issues/4) Add badges to README to improve initial look
