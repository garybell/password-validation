# Version 2.2.1
This is a minor release. It adds no new functionality to the password validator, but allows PHP 8.1 support.

* [#26](https://gitlab.com/garybell/password-validation/-/issues/26) Add CI pipeline job for PHP 8.1
* [#27](https://gitlab.com/garybell/password-validation/-/issues/27) Migrate the PhpUnit configuration to remove the warning in the console when running tests

# Version 2.1.0

This is a minor release, simply adding support for PHP 8

# Version 2.0.0

This is a major release and contains breaking changes over 0.x and 1.x versions. 
This release drops support for PHP 7.3 - now requiring PHP 7.4 or higher.

* [#12 - BREAKING CHANGE](https://gitlab.com/garybell/password-validation/-/issues/12) Change PHP requirements in composer.json to `^7.4`
* [#13 - BREAKING CHANGE](https://gitlab.com/garybell/password-validation/-/issues/13) Remove pipeline job for PHP 7.3. 
This is a breaking change as it will not validate the code works on PHP 7.3.
* [#19](https://gitlab.com/garybell/password-validation/-/issues/19) Create wiki to allow placement of historic usage instructions
* [#14](https://gitlab.com/garybell/password-validation/-/issues/14) Add link to usage instructions for historic versions
* [#16 - BREAKING CHANGE](https://gitlab.com/garybell/password-validation/-/issues/16) Change the `getLength` function to be static so it can be used without instantiating the whole class.
* [#15 - BREAKING CHANGE](https://gitlab.com/garybell/password-validation/-/issues/15) Change the `getBase` function to be static so it can be used without instantiating the whole class.
* [#17 - BREAKING CHANGE](https://gitlab.com/garybell/password-validation/-/issues/17) Change the `getEntropy` function to be static so it can be used without instantiating the whole class. 
This also returns a float, and takes an optional parameter for decimal places (defaults to 2).
* [#18](https://gitlab.com/garybell/password-validation/-/issues/18) Update the usage documentation within the README file.
Also remove the line about the pessimistic entropy, now the `getEntropy` function returns a float and not a rounded down integer.

# Version 1.0.1

This is a documentation release to add the missing parts, and correct erroneous parts of the 1.0.0 release.
It is expected that this is the final release which will support PHP 7.3.

* [#9](https://gitlab.com/garybell/password-validation/-/issues/9) Add v1.0.0 release notes to changelog
* [#10](https://gitlab.com/garybell/password-validation/-/issues/10) Fix badges for pipeline and code coverage within the README.md file

# Version 1.0.0

* [#5](https://gitlab.com/garybell/password-validation/-/issues/5) Add unit test pipeline for PHP 7.3
* [#4](https://gitlab.com/garybell/password-validation/-/issues/4) Add badges to README to improve initial look

# Version 0.1.2

* [#8](https://gitlab.com/garybell/password-validation/-/issues/8) Addition of concise changelog at the root of the project.
* [#2](https://gitlab.com/garybell/password-validation/-/issues/2) Update the README.md to include installation and usage instructions.
 
# Version 0.1.1

This release includes a few minor updates. No functionality changes are included.

* [#1](https://gitlab.com/garybell/password-validation/-/issues/1) Remove `phpunit.result.cache` from files. 
This is created when running unit tests locally.
* [#7](https://gitlab.com/garybell/password-validation/-/issues/7) Fix pipeline auto-release. 
This was missing a CI/CD pipeline variable, and the URL for the project in the `.gitlab-ci.yml` file was incorrect.
* [#3](https://gitlab.com/garybell/password-validation/-/issues/3) Add keywords to package.json. 
This will allow the password to be discovered more easily within packagist.
* [#6](https://gitlab.com/garybell/password-validation/-/issues/6) Add pipeline code for deploying to packagist.

## Additional Notes
Due to issue #7, version 0.1.0 was not actually tagged, or officially released.

# Version 0.1.0

* Add License. License is MIT to keep with the original source.
* Add CI/CD pipeline file to allow unit testing and auto releasing
* Add PHP unit testing ability
* Add README.md file to inform people of the project
* finish test coverage
