<?php
/**
 * DESCRIPTION
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package password-validator
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\PasswordValidator\Tests;


use GaryBell\PasswordValidator\PasswordValidator;

class EntropyTest extends \PHPUnit\Framework\TestCase
{
    public function testGetEntropyOfSimplePassword()
    {
        $password = "abc";
        $this->assertEquals(9.77, PasswordValidator::getEntropy($password));
    }

    public function testGetEntropyOfSimplePasswordDoubled()
    {
        $password = "abcabc";
        $this->assertEquals(19.55, PasswordValidator::getEntropy($password));
    }

    public function testGetEntropyOfSimplePasswordUpperAndLower()
    {
        $password = "abcABC";
        $this->assertEquals(23.71, PasswordValidator::getEntropy($password));
    }

    public function testEntropyOfUpperLowerAndNumbers()
    {
        $password = "abcABC123";
        $this->assertEquals(37.14, PasswordValidator::getEntropy($password));
    }

    public function testEntropyFullKnownSets()
    {
        $password = "abcABC123!%^";
        $this->assertEquals(54.52, PasswordValidator::getEntropy($password));
    }

    public function testEntropyXKCD()
    {
        $password = "correct horse battery staple";
        $this->assertEquals(85.27, PasswordValidator::getEntropy($password));
    }

    public function testEntropyStrongPassword()
    {
        $password = "*Mie0y1Fnfnw8Gb5ypt%";
        $this->assertEquals(90.87, PasswordValidator::getEntropy($password));
    }

    public function testEntropyKnownBannedPassword()
    {
        $this->assertEquals(0, PasswordValidator::getEntropy('1qaz2wsx3edc'));
        $this->assertEquals(0, PasswordValidator::getEntropy('q1w2e3r4t5y6'));
        $this->assertEquals(0, PasswordValidator::getEntropy('27653'));
        $this->assertEquals(0, PasswordValidator::getEntropy('bangbang123'));
        $this->assertEquals(0, PasswordValidator::getEntropy('ncc1701d'));
    }

    public function testEntropyCustomBannedList()
    {
        $this->assertEquals(0, PasswordValidator::getEntropy('Password1!',
            2,
            [
                'password1!',
                'password5%',
            ]
            )
        );
    }

    /**
     * Non-ASCII characters add multiple characters to the length
     * The ü uses 2 characters, so it's the same as the password abcuu
     */
    public function testGetEntropyWithInternationalCharacters()
    {
        $password = "abcü";
        $this->assertEquals(16.66, PasswordValidator::getEntropy($password));
    }
}
