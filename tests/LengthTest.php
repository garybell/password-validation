<?php
/**
 * DESCRIPTION
 *
 * (c) 2020 Gary Bell <gary@garybell.co.uk>
 *
 * @package password-validator
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace GaryBell\PasswordValidator\Tests;


use GaryBell\PasswordValidator\PasswordValidator;

class LengthTest extends \PHPUnit\Framework\TestCase
{
    public function testLength()
    {
        $password = 'aaaa';
        $this->assertEquals(2, PasswordValidator::getLength($password));
        $password = 'aaaabbbb';
        $this->assertEquals(4, PasswordValidator::getLength($password));
        $password = '12121234';
        $this->assertEquals(6, PasswordValidator::getLength($password));
    }

    public function testBaseXKCD()
    {
        $passwordValidator = new PasswordValidator();
        $password = "correct horse battery staple";
        $this->assertEquals(21, PasswordValidator::getLength($password));
    }
}
